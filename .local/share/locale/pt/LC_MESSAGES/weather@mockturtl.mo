��    C      4  Y   L      �     �     �     �     �     �     �     �  $   �                    "     (     9     G  	   N     X     ]  
   b     m  	   q  	   {     �     �     �     �     �     �     
          3     G     \     c     q       	   �     �     �     �     �     �     �     �                         &     3     :     @     H     O     ^     k     z     �     �     �     �  	   �     �     �     �     �  �  �     f
     j
     r
     �
     �
     �
     �
  "   �
     �
     �
     �
     �
     �
     �
                           (     5  	   <     F     O     d  
   y     �  -   �  -   �     �     �               +     9     G     X  	   m     w          �     �     �     �     �     �     �     �     �     �       
             '     3     G     T  
   `     k     x     �     �     �     �     �     �     �     4   3         1       C         8   %   *   0      /   #       >       (      .       6            ,      <   7      2   A                                                        '   :             9   B                 	      &      @             )   "          
   ;       -   ?                  =   5   +       !       $    ... Blowing snow Blustery Clear Click to open Cloudy Cold Display current temperature in panel Drizzle Dust Fair Foggy Freezing drizzle Freezing rain Friday Get WOEID Hail Haze Heavy snow Hot Humidity: Hurricane Isolated thundershowers Isolated thunderstorms Light snow showers Loading ... Loading current weather ... Loading future weather ... Mixed rain and hail Mixed rain and sleet Mixed rain and snow Mixed snow and sleet Monday Mostly cloudy Not available Partly cloudy Pressure: Saturday Scattered showers Scattered snow showers Scattered thunderstorms Settings Severe thunderstorms Showers Sleet Smoky Snow Snow flurries Snow showers Sunday Sunny Sunrise Sunset Symbolic icons Temperature: Thundershowers Thunderstorms Thursday Tornado Tropical storm Tuesday Wednesday Wind Chill: Wind speed unit Wind: Windy Project-Id-Version: 3.0.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2011-06-07 18:21+0100
Last-Translator: 
Language-Team: Português ; Daniel Miranda <dmiranda@gmail.com>, Luiz Rodrigo <contact@lurodrigo.com>, Zé Bento <bento2000@gmail.com>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bits
Plural-Forms: nplurals=2; plural=(n!=1);
 ... Nevasca Rajadas de vento Limpo Clique para abrir Nublado Frio Mostra temperatura atual no painel Chuvisco Neblina Limpo Neblina Geada Chuva congelada Sexta-feira Obter WOEID Granizo Neblina Forte nevada Quente Humidade: Furacão Tempestades isoladas Tempestades isoladas Neve fraca Carregando ... Carregando as condições meteorológicas ... Carregando as condições meteorológicas ... Chuva e granizo Granizo e chuva Chuva e neve Granizo e neve Segunda-feira Muito nublado Não disponível Parcialmente nublado Pressão: Sábado Chuvas Isoladas Nevadas isoladas Tempestades isoladas Preferências Tempestade severa Chuva Granizo Niebla Neve Flocos de neve Nevadas Domingo Ensolarado Nascer do sol Pôr do sol Ícones simbólicos Temperatura: Tempestades Tempestade Quinta-feira Tornado Tempestade tropical Terça-feira Quarta-feira Vento: Velocidade do vento Vento: Ventoso 