��          �      �       H  '   I     q  %   �     �     �  *   �     �  	   �  �   �          �     �     �    �  9   �       '   +     S     _  -   d     �     �  �   �     F     \     r  
   �     	                            
                                Click on the applet to turn off monitor Color of the symbolic icon Duration of deactivation of the mouse General Icon Keyboard shortcut to turn off your monitor Mouse Shortcuts To prevent the monitor from turning on too soon after turning it off, the mouse will be disabled for the number of seconds set here. Turn Off Monitor Turn off monitor Use a symbolic icon seconds Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-04 16:35+0200
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: Bosák Balázs <bossbob88@gmail.com>
Language-Team: 
X-Generator: Poedit 2.4.2
 Kattintson a kisalkalmazásra a monitor kikapcsolásához Szimbolikus ikon színe Az egér deaktiválásának időtartama Általános Ikon Billentyűparancs a monitor kikapcsolásához Egér Gyorsbillentyűk Annak megakadályozása érdekében, hogy a monitor kikapcsolása után túl hamar bekapcsoljon, az egér az itt beállított másodpercekig le lesz tiltva. Monitor kikapcsolása Monitor kikapcsolása Szimbolikus ikon használata másodperc 