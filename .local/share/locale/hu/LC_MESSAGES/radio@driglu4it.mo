��    5      �  G   l      �  "   �     �  
   �     �     �  1   �                4  +   D  3   p  6   �     �     �               ;     N  
   [     f     v  	   {     �     �     �  	   �     �  #   �  4   �  
   +  %   6      \     }     �     �  (   �     �     �     �  '        ;     H     M     Z     c     q     �     �     �     �     �  #   �  #  �     �	  "   
     7
     C
     O
  M   `
     �
     �
     �
  8   �
  8   8  B   q     �     �     �  .   �     *     E     `     o     �     �     �  !   �     �     �     �  )     N   =     �  "   �  "   �     �     �     �            9   2  3   l  >   �     �     �     �               +     B     G     N     R     [  >   u                             '   +      #                            4      $   %                 ,                    2   -                           *   )   /       0      "   &   1   3                                  5      	      
      !      .   (    A simple radio applet for Cinnamon Add to My Stations Appearance Bicolor Bicolor Icon Can't increase Volume. Volume already at maximum. Can't play  %s Cancel Installation Cancel download Color of Symbolic Icon when playing a radio Color of symbolic icon when playing a radio station Color of symbolic icon while a radio station is paused Copy current song title Download directory Download from Youtube Download plugin at my own risk Find Radio Station Find Station Full Color Full Color Icon Icon Icon type Initial volume Install at my own risk List of stations More info My Stations Open radio stream URL search engine Opens a website, which lists URLs of radio stations. Playing %s Please also install the '%s' package. Please install the '%s' package. Preferences Radio Radio++ Remember volume after stopping the radio Search Radio station See Logs for more information Show URL-list of radio stations Show current radio station on the panel Show in list Stop Stop Radio++ Symbolic Symbolic Icon Symbolic Icon Color Title Type URL Volume Youtube Download download finished. File saved to %s Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-12-23 09:06+0100
Last-Translator: Kálmán „KAMI” Szalai <kami911@gmail.com>
Language-Team: 
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.3
 Cinnamon rádió kisalkalmazás Saját rádióállomásokhoz adás Megjelenés Kétszínű Kétszínű ikon A hangerő már nem növelhető, mert a hangerő már a lehető leghangosabb. „%s” nem játszható le Telepítés megszakítása Letöltés megszakítása A szimbolikus ikon színe, amikor a rádiót játszik le A szimbolikus ikon színe, amikor a rádiót játszik le A szimbolikus ikon színe, amikor a rádió lejátszása szünetel Szám címének másolása Letöltési mappa Letöltés a YouTube-ról Bővítmény letöltése saját felelősségre Rádióállomás keresése Rádióállomás keresése Teljes színű Teljes színű ikon Ikon Ikon típus Kezdeti hangerő Telepítés saját felelősségre Rádióállomások listája További információ Saját rádióállomások Rádióállomás keresőmotor megnyitása Megnyit egy weboldalt, amely a rádióállmások URL listáját jeleníti meg. Lejátszás: %s Telepítse a(z) „%s” csomagot. Telepítse a(z) „%s” csomagot. Beállítások Rádió Radio++ Hangerő eltárolása Rádióállomás keresése További információkért tekintse meg a naplófájlokat Rádióállomások URL-listájának megjelenítése Aktuális rádióállomás nevének megjelenítése a Panel-on Listanézet Leállítás Radio++ leállítása Szimbolikus Szimbolikus ikon Szimbolikus ikon szín Cím Típus URL Hangerő Letöltés a YouTube-ról fájl letöltése sikerült. A fájl ide lett mentve: „%s” 