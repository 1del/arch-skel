��    	      d      �       �      �      �   B   �      7     G     O     ]     m  M  z     �     �  L   �     4     F     O     h     z                          	                    Effect End of Drag Fade out the current window that the user is resizing or dragging. Opacify Windows Opacity Start of Drag Transition time milliseconds Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-10-21 20:57+0200
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.1
Last-Translator: Dragone2 <dragone2@risposteinformatiche.it>
Plural-Forms: nplurals=2; plural=(n != 1);
Language: it
 Effetto Fine del Trascinamento Dissolve la finestra attuale che l'utente sta ridimensionando o trascinando. Opacizza Finestre Opacità Inizio del Trascinamento Tempo transizione millisecondi 