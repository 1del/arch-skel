��          �      |      �  
   �     �  )   	     3     A     S  
   [  �   f       5     4   H  (   }     �  :   �     �  	   �       B        ^     s     �  M  �     �     �  +        1     A     [     d  �   x     F  C   [  >   �  '   �       B        [     l     }  H   �     �     �     	                                                                             	            
        %s enabled Bottom panel Choose the type of transparency you want. Filter panels Fully transparent General Left panel Only for supported themes. Let the theme style the panels instead of using the extension presets. Use this in case the panels transparency is icorrectly set. Open settings Open the extension settings and customize your panels Remove panel transparency when it's not transparent. Remove theme transparency (experimental) Right panel Select the panels that will be affected by this extension. Semi-transparent Top panel Transparent panels Transparentize your panels when there are no any maximized windows Type of transparency Use current theme styles With shadow Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-10-21 21:16+0200
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.1
Last-Translator: Dragone2 <dragone2@risposteinformatiche.it>
Plural-Forms: nplurals=2; plural=(n != 1);
Language: it
 %s abilitato Pannello in basso Scegli il tipo di trasparenza che desideri. Filtro pannelli Completamente trasparente Generale Pannello a sinistra Solo per temi supportati. Lascia che il tema modifichi i pannelli anziché utilizzare i predefiniti dell'estensione. Utilizza questo nel caso in cui la trasparenza dei pannelli sia impostata correttamente. Apri le Impostazioni Apri le impostazioni dell'estensione e personalizza i tuoi pannelli Rimuove la trasparenza del pannello quando non è trasparente. Rimuovi trasparenza tema (sperimentale) Pannello a destra Seleziona i pannelli che saranno interessati da questa estensione. Semi-trasparente Pannello in alto Pannelli trasparenti Rendi trasparenti i tuoi pannelli quando non ci sono finestre ingrandite Tipo di trasparenza Usa gli stili del tema corrente Con ombreggiatura 