��          �   %   �      `  "   a     �     �  +   �  5   �                ?     O     T  	   e  #   o  @   �  
   �      �                                -     ;  �   O     �     �     �  K  �     9     O     _  0   r  :   �  #   �  3        6     F     K     `  /   r  K   �  
   �  $   �          $     ,     8     =     J     X  �   k     		     	     	                                         	   
                                                                                A simple radio applet for Cinnamon Bicolor Icon Cancel download Color of Symbolic Icon when playing a radio Do you want to proceed the download at your own risk? Download Confirmation Dialog Download plugin at my own risk Full Color Icon Icon List of stations More info Open radio stream URL search engine Opens a website, which helps you to determine radio stream URLs. Playing %s Please install the '%s' package. Radio Radio++ Show in list Stop Stop Radio++ Symbolic Icon Symbolic Icon Color The radio applet depends on the '%s' plugin. It's a 3rd party plugin for mpv, which allows controlling the radio player with the sound applet. Title Type URL Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-12-21 18:02+0100
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.3
Last-Translator: Alan Mortensen <alanmortensen.am@gmail.com>
Plural-Forms: nplurals=2; plural=(n != 1);
Language: da
 En enkel radiospiller Sort/hvidt ikon Annullér download Det symbolske ikons farve når der spilles radio Vil du fortsætte med download for egen regning og risiko? Dialog til bekræftelse af download Download udvidelsesmodul for egen regning og risiko Flarfarvet ikon Ikon Liste over stationer Flere oplysninger Åbn søgemaskine til URL'er til radiostationer Åbner et website, som hjælper dig med at finde URL'er til radiostationer. Spiller %s Installér venligst pakken “%s”. Radio Radio++ Vis i liste Stop Stop Radio++ Symbolsk ikon Symbolsk ikonfarve Panelprogrammet afhænger af udvidelsesmodulet “%s”. Det er et tredjepartsmodul til mpv, som tillader kontrol af radioafspilleren med lydpanelprogrammet. Titel Type URL 