��    E      D  a   l      �     �     �                              %     *     ,     1     7     H     V     ]     b  
   g     r  	   v  	   �     �     �     �     �     �     �          #     8     L     a     h     v     x     {     ~     �  	   �     �     �     �     �     �     �     �     �                         $     2     ?     F     L     T     [     h     w     �     �     �     �     �  	   �     �     �     �  �  �     V
     Z
  
   i
     t
  	   |
     �
     �
     �
     �
     �
     �
     �
     �
  
   �
     �
     �
     �
     
               (     E     b     s  *   �  &   �     �     �     �       	        %     :     <     ?     B     O  
   b     m     o     r  	   u          �     �     �     �     �     �          
       
        )  
   2  	   =     G     U     f     w     �     �     �     �  
   �     �     �     �     +   9             =      ?       '       .       D       &   E             /   <                       5      ,           0      	                    -   B             8      )                 2       #   *       !   A       C          3       6       
   (                1   @       "       $       %                 4      :   >          ;   7           ... Blowing snow Blustery Clear Cloudy Cold Drizzle Dust E Fair Foggy Freezing drizzle Freezing rain Friday Hail Haze Heavy snow Hot Humidity: Hurricane Isolated thundershowers Isolated thunderstorms Light snow showers Loading ... Loading current weather ... Loading future weather ... Mixed rain and hail Mixed rain and sleet Mixed rain and snow Mixed snow and sleet Monday Mostly cloudy N NE NW Not available Partly cloudy Pressure: S SE SW Saturday Scattered showers Scattered snow showers Scattered thunderstorms Severe thunderstorms Showers Sleet Smoky Snow Snow flurries Snow showers Sunday Sunny Sunrise Sunset Temperature: Thundershowers Thunderstorms Thursday Tornado Tropical storm Tuesday W Wednesday Wind Chill: Wind: Windy Project-Id-Version: 3.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-02-16 11:25+0100
Last-Translator: Reinis Danne <rei4dan@gmail.com>
Language-Team: 
Language: lv
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
X-Poedit-SourceCharset: utf-8
X-Generator: Poedit 2.3
 ... Sniega putenis Brāzmains Skaidrs Apmācies Auksts Smidzina Putekļi A Skaidrs Miglains Sasalstoša smidzināšana Sasalstošs lietus Piektdiena Krusa Dūmaka Spēcīgs sniegs Karsts Mitrums: Viesuļvētra Atsevišķi pērkona negaisi Atsevišķi pērkona negaisi Viegla snigšana Ielādē ... Ielādē pašreizējos laikapstākļus ... Ielādē gaidāmos laikapstākļus ... Lietus un krusa Slapjdraņķis Lietus un sniegs Sniegs un slapjš sniegs Pirmdiena Pārsvarā apmācies Z ZA ZR Nav pieejams Daļēji apmācies Spiediens: D DA DR Sestdiena Vietām lietusgāzes Vietām sniega putenis Vietām pērkona negaiss Spēcīgs pērkona negaiss Lietusgāzes Slapjš sniegs Dūmaka Sniegs Sniegputenis Sniegs Svētdiena Saulains Saullēkts Saulriets Temperatūra: Pērkona negaiss Pērkona negaiss Ceturtdiena Tornado Tropiskā vētra Otrdiena R Trešdiena Vējš: Vējš: Vējains 